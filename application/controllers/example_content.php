<?php
class example_content extends My_Base_Controller
{
	public function index()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$this->session->set_flashdata('success', 'Example message!!');
			redirect('/example_content');
		}
		else
		{
			// Serve up the page
			$this->load->view('header_view');
			$this->load->view('navigation_view');
			$this->load->view('example_content_view');
			$this->load->view('footer_view');
		}
	}
}
