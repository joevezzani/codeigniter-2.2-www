<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="/favicon.ico">
	
	<title>Above Market</title>
	
	<!-- J Query -->
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	
	<!-- Bootstrap core CSS -->
	<link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	
	<!-- Bootstrap theme -->
	<link href="/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
	
	<!-- Custom styles for this template -->
	<link href="/css/mycss.css" rel="stylesheet">
	<link href="/css/style.css" rel="stylesheet">
	
	<!-- Font awesome -->
	<link rel="stylesheet" href="/font-awesome-4.3.0/css/font-awesome.min.css">
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body role="document">
