<div class="container theme-showcase" role="main">
	<h1>Example Content View!</h1>
	<p>This is an example content view...</p>
	<form action="/example_content" method="post" accept-charset="UTF-8" enctype="application/x-www-form-urlencoded" autocomplete="off" novalidate>
	<input type="submit" value="Post some data..." />
	</form>
</div> <!-- /container -->