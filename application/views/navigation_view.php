<div id="navigation">
<?php if ($success = $this->session->flashdata('success')): ?>
	<div class="container">
		<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Success! </strong><?=$success?>
		</div>
	</div>
<?php endif; ?>

<?php if ($info = $this->session->flashdata('info')): ?>
	<div class="container">
		<div class="alert alert-info alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Info! </strong><?=$info?>
		</div>
	</div>
<?php endif; ?>

<?php if ($warning = $this->session->flashdata('warning')): ?>
	<div class="container">
		<div class="alert alert-warning alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Warning! </strong><?=$warning?>
		</div>
	</div>
<?php endif; ?>

<?php if ($danger = $this->session->flashdata('danger')): ?>
	<div class="container">
		<div class="alert alert-danger alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Error! </strong><?=$danger?>
		</div>
	</div>
<?php endif; ?>
</div>
